package bejeweled;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MyMouseListener extends MouseAdapter {
	
	private Pool pool;
	private Gem selected_gem;
	private GamePanel gp;
	private boolean pair;
	private Behavior bh;
	
	public MyMouseListener(Pool pool, GamePanel gp, Behavior bh) {
		this.pool = pool;
		this.gp = gp;
		this.pair = false;
		this.bh = bh;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
//		System.out.println("Mouse Clicked");
//		System.out.println(Integer.toString(e.getX())+ " + " + Integer.toString(e.getY()));
		int x = getRealX(e.getY());
		int y = getRealY(e.getX());
//		System.out.println("Get XY"+ Integer.toString(x) +" , " + Integer.toString(y));
		Gem g = pool.getGem(x, y);
//		System.out.println(g.getGemType());
		if (this.selected_gem == null) {
			this.pair = false;
			this.selected_gem = g;
			this.selected_gem.setSelected();
		} else if (this.selected_gem != g) {
			if (!this.pair) 
			pool.swapGem(selected_gem, g);
			this.pair = true;
			this.selected_gem = null;
			this.gp.repaint();
			this.bh.runLoop();
		}		
		
	}
	
	public int getRealX(int y) {
		int gx = 5 - (y-100)/80;
		return gx;
	}
	
	public int getRealY(int x) {
		int gy = (x-120)/80;
		return gy;
	}

}
