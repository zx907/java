package bejeweled;

import java.awt.EventQueue;
import javax.swing.border.EmptyBorder;

public class StartHere {
	
	private GamePanel contentPane;
	private Behavior bh;
	private Pool pool;
	private MyMouseListener l;	



	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StartHere sh = new StartHere();
					sh.initGame();
//					System.out.println("Going into sleep...");
//					Thread.sleep(5000);
//					System.out.println("Wake up");
					sh.Loops();
					
										
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	protected void initGame() {
		
		// Setup Framework
		Framework frame = new Framework();
		frame.setSize(900, 700);
		frame.setVisible(true);
		
		// Initialize pool
		this.pool = new Pool();
		pool.printPool();
		
		
		//Set Content Panel (GamePanel)
		contentPane = new GamePanel(pool,bh);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		frame.setContentPane(contentPane);
		
		
		// Initialized Behavior
		this.bh = new Behavior(pool,contentPane);
		
		l = new MyMouseListener(pool,contentPane, bh);
		contentPane.addMouseListener(l);
		
		
	}

	public void Loops() {
		bh.runLoop();
		bh.total_score_flag = true;	
	}	
	
}
