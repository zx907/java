package bejeweled;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Gem {

	public enum enumGemType {DEER, LION, OWL, RACCOON, WHALE};
	public static final int UNITLENGTH = 80;
	
	
	private int x;
	private int y;
	public int geo_x;
	public int geo_y;
	private enumGemType gem_type;
	private boolean isDel;
	@SuppressWarnings("unused")
	private boolean toDrop;
	private int drop_distance;
	private boolean selected;
	
	
	// Gem constructor. Position (x, y) and Type
	public Gem(int x, int y, enumGemType gem_type) {
		this.x = x;
		this.y = y;
		this.geo_x = 100 + 80 * this.y;
		this.geo_y = 500 - 80 * this.x;
		this.gem_type = gem_type;
		this.isDel = false;
		this.toDrop = false;
		this.drop_distance = 0;
		this.selected = false;
	}
	
	
	public enumGemType getGemType() {
		return this.gem_type;
	}	
	
	public void changeGemPosition(int x, int y) {
		this.x = x;
		this.y = y;
		this.updateGemGeoPosition();
	}	
	
	public int getGemPositionX() {
		return this.x;
	}
	
	public int getGemPositionY() {
		return this.y;
	}
	
	public boolean markDel() {
		return this.isDel = true;
	}
	
	public boolean isDel() {
		return this.isDel;
	}
	
	public void setToDrop(int h) {
		this.drop_distance = h;
		this.toDrop = true;
	}
	
	public int getDropHeight() {
		return this.drop_distance;
	}
	
	public void clearAllTag() {
		this.isDel = false;
		this.drop_distance = 0;
		this.toDrop = false;
	}
	
	public void updateGemGeoPosition() {
		this.geo_x = 100 + 80 * this.y;
		this.geo_y = 500 - 80 * this.x;
	}
	
	public void draw(Graphics g) {
			g.drawImage(getGemImage(this.gem_type), this.geo_x	, this.geo_y, null);
	}
	
	private BufferedImage getGemImage(enumGemType type) {
		BufferedImage img;
//		String tmp_img_loc = "/res1/".concat(type.toString().concat(".png"));
//		System.out.println(tmp_img_loc);

		try {
			img = ImageIO.read(getClass().getResource(type.toString().concat(".png")));
//			img = ImageIO.read(getClass().getResourceAsStream(tmp_img_loc));
			return img;
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("cannot load gem image");
			return null;
		}
		
		
	}

	public void setSelected() {
		this.selected = true;
	}

	public boolean isSelected() {
		return selected;
	}
		
}
