package bejeweled;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;



@SuppressWarnings("serial")
public class GamePanel extends JPanel {


	private BufferedImage img_bg;
	private BufferedImage img_grid;

	private Pool pool;
	

	/**
	 * Create the frame.
	 */
	public GamePanel(Pool pool, Behavior bh) {
		
		this.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setLayout(new BorderLayout(0, 0));
		
		this.pool = pool;		
		
        try {
            img_bg = ImageIO.read(getClass().getResource("nebula.jpg"));
            img_grid = ImageIO.read(getClass().getResource("grid.png"));
        } catch (IOException e) {
        }
	
		
	}	
	
	/* 
	 * Paint background image and grid to panel(non-Javadoc)
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
    public void paintComponent(java.awt.Graphics g) {
//    	System.out.println("Start painting");
    	super.printComponents(g);
    	g.drawImage(img_bg, 0, 0, null);
    	g.drawImage(img_grid, 100, 100, null);
    	g.setFont(new Font("SERIF", Font.PLAIN, 30));
    	String tString = "TOTAL SCORE: "+ Integer.toString(Behavior.total_score);
    	g.setColor(Color.green);
    	g.drawString(tString, 300, 50);

    	
    	
    	// paint gem pool
//    	System.out.println("try to repaint pool");
    	for(int row=0;row<6;row++) {
    		for(int col=0;col<8;col++) {
//    			System.out.println(Integer.toString(row)+Integer.toString(col));
    			pool.getGem(row,col).draw(g);

//    			System.out.println(Integer.toString(pool.getGem(i,j).geo_x) + "," + Integer.toString(pool.getGem(i,j).geo_y));
    		}
    	}
    }

}
