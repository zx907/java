package bejeweled;

import java.util.ArrayList;
import java.util.Arrays;


public class Behavior {
	
	private ArrayList<ArrayList<Gem>> removed_list;
	private Pool pool;
	public static int total_score;
	private int[] fill_count;
	private GamePanel gp;
	public boolean total_score_flag; 

	
	// Constructor
	@SuppressWarnings("static-access")
	public Behavior(Pool pool, GamePanel gp) {
		this.removed_list = new ArrayList<ArrayList<Gem>>();
		this.pool = pool;
		this.total_score = 0;
		this.fill_count = new int[8]; // indicate how many gems to fill in each column
		this.gp = gp;
		this.total_score_flag = false;

	}
	
	
	/* 
	 * Loop designed to check current pool
	 */
	public void runLoop() {
		while (!this.isStable()) {
//			System.out.println("Not Stable");
			this.tagGem();
			this.calculateDropHeight();
			this.startDrop();
			if(total_score_flag)
				this.comboScore();				
			this.pushGem();
			this.removed_list.clear();
			Arrays.fill(this.fill_count, 0);
		} 
//		System.out.println("Stable");
		gp.repaint();
		
	}
	
	
	// Check if gem pool is stable
	public boolean isStable() {
		
		this.checkRow();
		this.checkColumn(); 
		if (removed_list.isEmpty()) 
			return true;
		else
			return false;
	}
	
	/* Find all chain in a row that can be deleted
	 * add found chain into a chain array
	 */
	public void checkRow() {

		ArrayList<Gem> row_chain = new ArrayList<Gem>();

		for (int row = 0; row < 6; row++) {
//			System.out.println("checking row " + Integer.toString(row));
			Gem start_gem = pool.getGem(row, 0);
			row_chain.clear();

			
			for (int col = 1; col < 8; col++) {
//				System.out.println("checking col " + Integer.toString(col));
				
				Gem gem = pool.getGem(row, col);
//				System.out.println("Successfully get gem"+gem.getGemType());
				
				if (gem.getGemType() == start_gem.getGemType()) {
					if (row_chain.isEmpty()) row_chain.add(start_gem);
					row_chain.add(gem);					
					if (col == 7) {
						if (row_chain.size() > 2) removed_list.add(new ArrayList<Gem>(row_chain));
						row_chain.clear();		
					}
				} else {
					if (row_chain.size() > 2) removed_list.add(new ArrayList<Gem>(row_chain));
					row_chain.clear();
					
				} start_gem = gem;
			}
		}
		
//		System.out.println("check row pass!");
	}
	
	/* Find all chain in a column that can be deleted
	 * add found chain into a chain array
	 */
	public void checkColumn() {
		ArrayList<Gem> column_chain = new ArrayList<Gem>();

		for (int col = 0; col < 8; col++) {
			Gem start_gem = pool.getGem(0, col);
			column_chain.clear();
//			System.out.println("checking col " + Integer.toString(col));
			
			for (int row = 1; row < 6; row++) {
//				System.out.println("checking row " + Integer.toString(row));
				Gem gem = pool.getGem(row, col);
				
				if (gem.getGemType() == start_gem.getGemType()) {
					if (column_chain.isEmpty()) column_chain.add(start_gem);
					column_chain.add(gem);
					if (row == 5) {
//						System.out.println("WTF");
						if (column_chain.size() > 2) removed_list.add(new ArrayList<Gem>(column_chain));
						column_chain.clear();						
					}
				} else {
					if (column_chain.size() > 2) removed_list.add(new ArrayList<Gem>(column_chain));
					column_chain.clear();
				}
				start_gem = gem;
			}
		}
//		System.out.println("check column pass!");
	}
	
	// Calculate total score in one move chain
	@SuppressWarnings("static-access")
	public void comboScore() {
		for (ArrayList<Gem> alg: removed_list) {
			int one_combo_score = alg.size() * 100;
			this.total_score += one_combo_score;
//			System.out.println("Total score is " + Integer.toString(this.total_score));
		}
	}
	
	// Tag those gems to be deleted
	public void tagGem() {
		for(ArrayList<Gem> alg: removed_list) {
			for(Gem g: alg) {
				pool.pool[g.getGemPositionX()][g.getGemPositionY()].markDel();
			}
		}
	}
	
	// Delete all tagged gems and set drop tag and distance
	public void calculateDropHeight() {		
		for(int col=0;col<8;col++) {
			int count = 0;
			boolean tag = false;
			for(int row=0;row<6;row++) {
				if (pool.pool[row][col].isDel()) {
					if (tag==false) count = 0;
					count++;
					tag = true;
					continue;
				} else if (!pool.pool[row][col].isDel() && tag)
					pool.pool[row][col].setToDrop(count);
			} 
				
		}
	}
	
	public void startDrop() {
		for (int col=0;col<8;col++) {

			for(int row=0;row<6;row++) {

				Gem g = pool.pool[row][col];
				int height = g.getDropHeight();
				if(g.isDel())
					this.fill_count[col]++;
				if(height != 0) {
					
					this.fill_count[col] = g.getDropHeight();
					
					pool.pool[row-height][col] = g;
					int temp = row - height;
					g.changeGemPosition(temp, col);
					g.updateGemGeoPosition();
					g.clearAllTag();
				}
			} 
		}
	}
	
	public void pushGem() {
		for (int col=0;col<8;col++) {
			int i = this.fill_count[col];
			while(i != 0) {
				pool.placeRandomGem(6-i, col);
				i--;			
			}
		}
	}
		
}
