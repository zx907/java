package bejeweled;

import java.util.Random;
import bejeweled.Gem.enumGemType;

public class Pool {
		
	public Gem[][] pool = new Gem[6][8];
	
	public Pool() {
		for(int row=0;row<6;row++) {
			for(int col=0;col<8;col++) {
				this.pool[row][col] = getRandomGem(row,col);
			}
		} 
//		System.out.println("Random pool initiallized successfully");
		
	}
	
	public void printPool() {
		for(int row=0;row<6;row++) {
			for(int col=0;col<8;col++) {
//				System.out.println(Integer.toString(row) + Integer.toString(col) + this.pool[row][col].getGemType());
			}
		}
	}


	// Get a gem at position x,y with random type
	public Gem getRandomGem(int x, int y) {
		Random rand = new Random();
		return new Gem(x,y,enumGemType.values()[rand.nextInt(5)]);
	}
	
	
	// swap gem, change gems in pool, and change gem attributes
	public void swapGem(Gem g1, Gem g2) {
		if (this.isNeighbor(g1, g2)) {
			int temp_x = g1.getGemPositionX();
			int temp_y = g1.getGemPositionY();
			g1.changeGemPosition(g2.getGemPositionX(), g2.getGemPositionY());
			g2.changeGemPosition(temp_x, temp_y);
			pool[g1.getGemPositionX()][g1.getGemPositionY()] = g1;
			pool[g2.getGemPositionX()][g2.getGemPositionY()] = g2;
//			System.out.println("swap successfully");
		}
//		System.out.println("skip swap");
	}
	
	
	// place a random gem at position x,y
	public void placeRandomGem(int x, int y) {
		Gem g = this.getRandomGem(x, y);
		pool[x][y] = g;
	}
	
	
	// place a gem
	public void placeGem(Gem g, int x, int y) {
		g.changeGemPosition(x, y);
		pool[x][y] = g;
	}
	
	
	// Get the gem at position x,y in pool
	public Gem getGem(int x, int y) {
		return pool[x][y];
	}
	
	public boolean isNeighbor(Gem g1, Gem g2) {
		if ((g1.getGemPositionX() == g2.getGemPositionX()) && 
				(Math.abs(g1.getGemPositionY() - g2.getGemPositionY()) == 1)) {
//			System.out.println("iN 1");
			return true;
		}
		else if (g1.getGemPositionY() == g2.getGemPositionY() &&
				(Math.abs(g1.getGemPositionX()-g2.getGemPositionX()) == 1)) {
//			System.out.println("iN 2");
			return true;
		}
		else {
//			System.out.println("iN 3");
			return false;	
		}
	}
	

}
